const Header = () => {
  return (
    <header>
      <h1 className="title">Bright Memory</h1>
    </header>
  );
};

export default Header;
