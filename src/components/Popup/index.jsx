import "@/app/style.css";

const Popup = ({ setClosePopup }) => {
  return (
    <div className="popup">
      <h2>&ldquo; Thank you for visiting me and remembering me.&rdquo;</h2>
      <h3>Svetlana Alayeva</h3>
      <button onClick={() => setClosePopup(true)}>Continue</button>
    </div>
  );
};

export default Popup;
