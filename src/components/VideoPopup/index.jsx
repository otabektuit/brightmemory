import useGallery from "@/hooks/useGallery";
import "./style.scss";

import { FaRegWindowClose } from "react-icons/fa";
import { useEffect, useState } from "react";

const VideoPopup = ({ path, close }) => {
  const { videoUrl: getVideoURL } = useGallery();
  const [videoUrl, setVideoUrl] = useState();

  useEffect(() => {
    const fetchVideoUrl = async () => {
      const url = await getVideoURL();
      setVideoUrl(url);
    };

    fetchVideoUrl();
  }, [path]);

  return (
    <div className="video-popup">
      <div className="video-container">
        <video src={videoUrl} controls />
        <button onClick={close}>
          <FaRegWindowClose />
        </button>
      </div>
    </div>
  );
};

export default VideoPopup;
