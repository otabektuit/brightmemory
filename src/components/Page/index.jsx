"use client";

import {
  buttonGroupVariantA,
  counterVariantA,
  datesVariantA,
  h2NameVariantA,
  tombImageVariantA
} from "@/animations";
import { useEffect, useMemo, useState } from "react";
import { motion } from "framer-motion";
import { getDate, getTime, isDateOlderThanOneDay } from "@/utils/time";
import { useSearchParams } from "next/navigation";

import Clock from "@/components/Clock";
import DateTransition from "@/components/DateTransition";
import Image from "next/image";
import RememberMeButton from "@/components/RememberMeButton";
import List from "@/components/List";
import RoundedButton from "@/components/RoundedButton";
import Link from "next/link";
import useRecords from "@/hooks/useRecords";
import useGallery from "@/hooks/useGallery";
import VideoPlayer from "../VideoPlayer";

const startDate = new Date("1971-05-16").getTime();
const endDate = new Date("2022-05-20").getTime();

const MotionImage = motion(Image);

const Page = ({ isVisitor }) => {
  const searchParams = useSearchParams();
  const { getRecords, saveRecord } = useRecords();

  const urlIsAnimationFinished = searchParams.get("isAnimationFinished");

  const [data, setData] = useState(null);
  const [count, setCount] = useState(0);

  const [countForLast30Days, setCountForLast30Days] = useState(0);
  const [isAnimationFinished, setIsAnimationFinished] = useState(
    !!urlIsAnimationFinished
  );

  const fetchData = async () => {
    try {
      const newData = await getRecords();

      setData(newData?.data);
      setCount(newData?.count);
      setCountForLast30Days(newData?.dataForLast30Days);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    setIsAnimationFinished(urlIsAnimationFinished);
  }, [urlIsAnimationFinished]);

  useEffect(() => {
    if (!isAnimationFinished) return;

    fetchData();
  }, [isAnimationFinished]);

  // to detect visits from qr-code
  useEffect(() => {
    if (isVisitor) saveRecord(true);
  }, [isVisitor]);

  const modifiedData = useMemo(() => {
    if (!data) return [];

    return data?.map(
      (item) =>
        `Someone ${
          item.checkIn ? "visited" : "rememberd"
        } Svetlana </br> at ${getTime(item?.date?.seconds)} on ${getDate(
          item?.date?.seconds
        )}`
    );
  }, [data]);

  const onEarlyRemember = () => {
    alert(
      "You already lit a candle for me today, thank you. Please come back tomorrow to remember me."
    );
  };

  const onRemember = async () => {
    const dateFromLS = localStorage.getItem("date");

    if (!isDateOlderThanOneDay(dateFromLS)) return onEarlyRemember();

    const res = await saveRecord();

    if (!res) return;

    fetchData();

    localStorage.setItem("date", JSON.stringify(new Date()));

    alert("Thank you for remembering me and lighting a candle in my memory.");
  };

  const onFinish = async () => {
    setIsAnimationFinished(true);
  };

  const isDesktop = typeof window !== "undefined" && window?.innerWidth > 768;

  return (
    <div
      key={"container_" + isAnimationFinished}
      className="clock-container"
      style={{
        maxHeight: !isAnimationFinished ? "calc(100vh - 72px)" : "auto",
        overflow: !isAnimationFinished ? "hidden" : "auto"
      }}
    >
      <MotionImage
        src={require("../../assets/tomb.svg")}
        className="tomb"
        {...tombImageVariantA(isAnimationFinished)}
      />

      {!isAnimationFinished && (
        <button
          className="skip-button"
          onClick={() => setIsAnimationFinished(true)}
        >
          Skip
        </button>
      )}

      <main>
        <Clock onFinish={onFinish} isAnimationFinished={isAnimationFinished} />

        <motion.h2
          className="fullname"
          {...h2NameVariantA(isAnimationFinished, isDesktop)}
          style={{
            "--glow-brightness": (countForLast30Days * 2) / 100
          }}
        >
          Svetlana <br /> Alayeva
        </motion.h2>

        <DateTransition
          startDate={startDate}
          endDate={endDate}
          isAnimationFinished={isAnimationFinished}
        />

        {!isAnimationFinished && <VideoPlayer />}

        <div className="dates">
          <motion.p {...datesVariantA(isAnimationFinished, isDesktop)}>
            May 16, 1971
          </motion.p>
          <motion.p {...datesVariantA(isAnimationFinished, isDesktop)}>
            May 20, 2022
          </motion.p>
        </div>
      </main>

      <motion.div
        className="candle-counter"
        {...counterVariantA(isAnimationFinished, isDesktop)}
      >
        <Image src={require("../../assets/candle.svg")} alt="candle" />
        <span style={{ marginRight: 10 }}>Remembered</span>
        <span>{countForLast30Days}</span>
      </motion.div>

      <motion.div
        className="list-container"
        {...tombImageVariantA(isAnimationFinished)}
        transition={{
          ...tombImageVariantA(isAnimationFinished).transition,
          duration: 2.4
        }}
      >
        <List items={modifiedData} />

        {count > 0 && (
          <p style={{ paddingBottom: 50 }}>...{count} more remembers</p>
        )}
      </motion.div>

      <motion.div
        {...buttonGroupVariantA(isAnimationFinished)}
        className="button-group"
      >
        <Link href="/gallery">
          <RoundedButton>
            <Image
              src={require("../../assets/image.svg")}
              width={30}
              height={30}
            />
          </RoundedButton>
        </Link>
        <RememberMeButton className="candle-button" onClick={onRemember} />

        <RoundedButton
          style={{
            opacity: 0
          }}
        >
          <Image
            src={require("../../assets/lock.svg")}
            width={30}
            height={30}
          />
        </RoundedButton>
      </motion.div>
    </div>
  );
};

export default Page;
