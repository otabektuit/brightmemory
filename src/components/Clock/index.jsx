"use client";

import { useEffect, useRef, useState } from "react";
import { motion } from "framer-motion";
import { clockVariantA } from "@/animations";

import Image from "next/image";
import styles from "./style.module.scss";

const maxRotateHour = 370;
const maxRotateMinute = 3048;

export default function Clock({ onFinish, isAnimationFinished }) {
  const minuteHandle = useRef(null);
  const hourHandle = useRef(null);

  const [minute, setMinute] = useState(228 / 6);
  const [hour, setHour] = useState(10 / 3);
  const [intervalTime, setIntervalTime] = useState(115);

  useEffect(() => {
    if (isAnimationFinished) return;

    const interval = setInterval(() => {
      setMinute((prev) => prev + 0.85);
      setHour((prev) => prev + 1 / 60);
    }, intervalTime);

    return () => {
      clearInterval(interval);
    };
  }, []);

  useEffect(() => {
    if (isAnimationFinished) return;

    const rotateMinute = minute * 6;
    const rotateHour = hour * 30;

    if (rotateMinute >= 3468 - 415) {
      onFinish();
      return;
    }

    minuteHandle.current.style.transform = `rotateZ(${rotateMinute}deg) scale(0.8)`;
    hourHandle.current.style.transform = `rotateZ(${rotateHour}deg)`;
  }, [minute, hour]);

  const isDesktop = typeof window !== "undefined" && window.innerWidth > 768;

  return (
    <motion.div
      {...clockVariantA(isAnimationFinished, isDesktop)}
      className={styles.container}
    >
      <div className={styles.wrapper}>
        <Image
          src={require("../../assets/clock.svg")}
          className={styles.clock}
        />
        <Image
          ref={hourHandle}
          src={require("../../assets/hour.svg")}
          className={styles.hour}
          style={{
            transform: `rotateZ(${
              isAnimationFinished ? maxRotateHour : 100
            }deg)`
          }}
        />
        <Image
          ref={minuteHandle}
          src={require("../../assets/minute.svg")}
          className={styles.minute}
          style={{
            transform: `rotateZ(${
              isAnimationFinished ? maxRotateMinute : 228
            }deg)`
          }}
        />
      </div>
    </motion.div>
  );
}
