import classNames from "classnames";

const RoundedButton = ({ children, className, ...props }) => {
  return (
    <button className={classNames("rounded-button", className)} {...props}>
      {children}
    </button>
  );
};

export default RoundedButton;
