const List = ({ items }) => {
  return (
    <ul className="list">
      {items.map((item, i) => (
        <li key={i} dangerouslySetInnerHTML={{ __html: item }}></li>
      ))}
    </ul>
  );
};

export default List;
