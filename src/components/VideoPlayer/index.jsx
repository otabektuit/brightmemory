import { TIME_TO_FINISH } from "@/animations";
import useGallery from "@/hooks/useGallery";
import { motion, useAnimation } from "framer-motion";
import React, { useCallback, useEffect, useRef, useState } from "react";

import { FaVolumeUp, FaVolumeMute } from "react-icons/fa";

const VideoPlayer = () => {
  const ref = useRef();
  const controls = useAnimation();

  const { videoUrl: getVideoURL } = useGallery();

  const [isVisible, setIsVisible] = useState(true);
  const [videoUrl, setVideoUrl] = useState("");
  const [muted, setMuted] = useState(true);

  const playVideo = useCallback(() => {
    if (!ref.current) return;

    ref.current.play();
  }, [ref.current]);

  useEffect(() => {
    const timer = setTimeout(() => {
      setIsVisible(false);
    }, TIME_TO_FINISH * 1000);

    return () => clearTimeout(timer);
  }, []);

  useEffect(() => {
    if (isVisible) {
      controls.start({ opacity: 1 });
    } else {
      controls.start({ opacity: 0 });
    }
  }, [isVisible, controls]);

  useEffect(() => {
    const fetchVideoUrl = async () => {
      const url = await getVideoURL();
      setVideoUrl(url);
    };

    fetchVideoUrl();

    const timeout = setTimeout(() => {
      playVideo();
    }, 1000);

    return () => {
      clearTimeout(timeout);
    };
  }, [ref.current]);

  return (
    <div className="video">
      <div className="video-wrapper">
        <motion.video
          initial={{ opacity: 0 }}
          animate={controls}
          transition={{ duration: 1 }}
          ref={ref}
          src={videoUrl}
          width={600}
          playsInline
          muted={muted}
        />
        <button className="volume" onClick={() => setMuted((prev) => !prev)}>
          {muted ? <FaVolumeMute /> : <FaVolumeUp />}
        </button>
      </div>
    </div>
  );
};

export default VideoPlayer;
