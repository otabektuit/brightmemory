import classNames from "classnames";
import styles from "./style.module.scss";
import Image from "next/image";

const RememberMeButton = ({ className, ...props }) => {
  return (
    <button className={classNames(styles.button, className)} {...props}>
      <Image src={require("../../assets/candle.svg")} /> Remember me
    </button>
  );
};

export default RememberMeButton;
