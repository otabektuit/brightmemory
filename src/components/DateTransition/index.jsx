import { TIME_TO_FINISH } from "@/animations";
import { motion } from "framer-motion";
import moment from "moment";
import React, { useState, useEffect } from "react";
import { useSpring, animated } from "react-spring";

const DateTransition = ({ startDate, endDate, isAnimationFinished }) => {
  const [count, setCount] = useState(0);

  const [intervalMs, setIntervalMs] = useState(
    isAnimationFinished ? 1 : TIME_TO_FINISH * 1000
  );

  const props = useSpring({
    number: endDate,
    from: { number: startDate },
    config: {
      duration: intervalMs,
      tension: 20,
      friction: 1000,
      easing: (t) => {
        return t < 0.5 ? 8 * t * t * t * t : 1 - Math.pow(-2 * t + 2, 4) / 2;
      }
    },
    reset: false
  });

  useEffect(() => {
    const interval = setInterval(() => {
      setCount((prev) => prev + 1);
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  useEffect(() => {}, [count]);

  return (
    <motion.div
      initial={{ opacity: isAnimationFinished ? 0 : 1 }}
      animate={{ opacity: 0 }}
      transition={{
        delay: TIME_TO_FINISH,
        duration: 1
      }}
    >
      <animated.div className="animated-time">
        {props.number.interpolate((value) => {
          return moment(value).format("MMMM DD, YYYY");
        })}
      </animated.div>
    </motion.div>
  );
};

export default DateTransition;
