import { firestore, storage } from "@/utils/firebase";
import { collection, getDocs } from "firebase/firestore";
import { ref, getDownloadURL } from "firebase/storage";

const useGallery = () => {
  const getImages = async () => {
    try {
      const galleryRef = collection(firestore, "gallery");

      const querySnapshot = await getDocs(galleryRef);

      const data = querySnapshot.docs.map((doc) => doc.data());

      return data;
    } catch (error) {
      console.error(error);

      return [];
    }
  };

  const videoUrl = async (path) => {
    try {
      const storageRef = ref(storage, path || "video/main.MOV");

      const downloadUrl = await getDownloadURL(storageRef);

      return downloadUrl;
    } catch (error) {
      // Handle any errors
      console.log(error);
      console.log(error?.code);
      console.log(error?.message);
    }
  };

  return {
    getImages,
    videoUrl
  };
};

export default useGallery;
