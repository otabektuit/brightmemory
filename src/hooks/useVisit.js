import { firestore } from "@/utils/firebase";
import {
  addDoc,
  collection,
  getDocs,
  limit,
  orderBy,
  query,
  getCountFromServer
} from "firebase/firestore";

const useVisit = () => {
  const getVisits = async () => {
    try {
      const visitsRef = collection(firestore, "visitors");
      const q = query(visitsRef, orderBy("date", "desc"), limit(20));

      const querySnapshot = await getDocs(q);

      const data = querySnapshot.docs.map((doc) => doc.data());
      const allData = await getDocs(visitsRef);

      return { data, count: allData.size - data.length };
    } catch (error) {
      console.error(error);

      return [];
    }
  };

  const saveVisit = async () => {
    try {
      const newDoc = await addDoc(collection(firestore, "visitors"), {
        date: new Date()
      });

      return newDoc;
    } catch (err) {
      console.log(err);

      return null;
    }
  };
  return {
    getVisits,
    saveVisit
  };
};

export default useVisit;
