import { firestore } from "@/utils/firebase";
import {
  addDoc,
  collection,
  getDocs,
  limit,
  orderBy,
  query,
  where
} from "firebase/firestore";

const useRecords = () => {
  const getRecords = async () => {
    try {
      const recordsRef = collection(firestore, "records");

      const thirtyDaysAgo = new Date();
      thirtyDaysAgo.setDate(thirtyDaysAgo.getDate() - 30);

      const q = query(recordsRef, orderBy("date", "desc"), limit(20));
      const queryForLast30Days = query(
        recordsRef,
        orderBy("date", "desc"),
        where("date", ">=", thirtyDaysAgo),
        limit(50)
      );

      const querySnapshot = await getDocs(q);
      const allData = await getDocs(recordsRef);
      const dataForLast30Days = await getDocs(queryForLast30Days);

      const data = querySnapshot.docs.map((doc) => doc.data());

      return {
        data,
        count: allData.size - data.length,
        dataForLast30Days: dataForLast30Days.docs
          .map((doc) => doc.data())
          .filter((item) => !item.checkIn).length
      };
    } catch (error) {
      console.error(error);

      return [];
    }
  };

  const saveRecord = async (isCheckIn = false) => {
    try {
      const newDoc = await addDoc(collection(firestore, "records"), {
        date: new Date(),
        checkIn: isCheckIn
      });

      return newDoc;
    } catch (err) {
      console.log(err);

      return null;
    }
  };
  return {
    getRecords,
    saveRecord
  };
};

export default useRecords;
