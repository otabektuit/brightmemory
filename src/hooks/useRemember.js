import { firestore } from "@/utils/firebase";
import {
  addDoc,
  collection,
  getDocs,
  limit,
  orderBy,
  query,
  where
} from "firebase/firestore";

const useRemember = () => {
  const getRemembers = async () => {
    try {
      const remembersRef = collection(firestore, "remembers");

      const thirtyDaysAgo = new Date();
      thirtyDaysAgo.setDate(thirtyDaysAgo.getDate() - 30);

      const q = query(remembersRef, orderBy("date", "desc"), limit(20));
      const queryForLast30Days = query(
        remembersRef,
        orderBy("date", "desc"),
        where("date", ">=", thirtyDaysAgo),
        limit(50)
      );

      const querySnapshot = await getDocs(q);
      const allData = await getDocs(remembersRef);
      const dataForLast30Days = await getDocs(queryForLast30Days);

      const data = querySnapshot.docs.map((doc) => doc.data());

      return {
        data,
        count: allData.size - data.length,
        dataForLast30Days: dataForLast30Days.size
      };
    } catch (error) {
      console.error(error);

      return [];
    }
  };

  const saveRemember = async () => {
    try {
      const newDoc = await addDoc(collection(firestore, "remembers"), {
        date: new Date()
      });

      return newDoc;
    } catch (err) {
      console.log(err);

      return null;
    }
  };
  return {
    getRemembers,
    saveRemember
  };
};

export default useRemember;
