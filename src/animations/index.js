export const TIME_TO_FINISH = 64;

export const tombImageVariantA = (noNeedToAnimation) => ({
  animate: {
    opacity: 1
  },
  initial: {
    opacity: 0
  },
  transition: {
    delay: noNeedToAnimation ? 2 : TIME_TO_FINISH + 1.5,
    duration: 2
  }
});

export const h2NameVariantA = (noNeedToAnimation, isDesktop) => ({
  animate: {
    y: isDesktop ? 382.5 : 212.5
  },
  initial: {
    y: 0
  },
  transition: {
    delay: noNeedToAnimation ? 2 : TIME_TO_FINISH,
    duration: 2
  }
});

export const datesVariantA = (noNeedToAnimation, isDesktop) => ({
  animate: {
    y: isDesktop ? 200 : 10,
    opacity: 1
  },
  initial: {
    y: 0,
    opacity: 0
  },
  transition: {
    delay: noNeedToAnimation ? 2 : TIME_TO_FINISH + 3,
    duration: 1.5
  }
});

export const counterVariantA = (noNeedToAnimation, isDesktop) => ({
  animate: {
    y: isDesktop ? 400 : 200,
    opacity: 1
  },
  initial: {
    y: isDesktop ? 250 : 100,
    opacity: 0
  },
  transition: {
    delay: noNeedToAnimation ? 2 : TIME_TO_FINISH + 3,
    duration: 1.5
  }
});

export const clockVariantA = (noNeedToAnimation, isDesktop) => ({
  animate: {
    y: isDesktop ? 331.5 : 191.3,
    scale: isDesktop ? 1.18 : 0.9
  },
  initial: {
    y: 0,
    scale: 1
  },
  transition: {
    delay: noNeedToAnimation ? 2 : TIME_TO_FINISH,
    duration: 2
  }
});

export const buttonGroupVariantA = (noNeedToAnimation) => ({
  animate: {
    y: 0,
    opacity: 1
  },
  initial: {
    y: 10,
    opacity: 0
  },
  transition: {
    delay: noNeedToAnimation ? 2 : TIME_TO_FINISH + 4,
    duration: 1.5
  }
});
