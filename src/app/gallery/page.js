"use client";

import useGallery from "@/hooks/useGallery";
import Image from "next/image";
import Link from "next/link";
import LightGallery from "lightgallery/react";
import { useEffect, useState } from "react";

// import styles
import "lightgallery/css/lightgallery.css";
import "lightgallery/css/lg-zoom.css";
import "lightgallery/css/lg-thumbnail.css";

// If you want you can use SCSS instead of css
import "lightgallery/scss/lightgallery.scss";
import "lightgallery/scss/lg-zoom.scss";
import "./style.scss";

// import plugins if you need
import lgThumbnail from "lightgallery/plugins/thumbnail";
import lgZoom from "lightgallery/plugins/zoom";
import VideoPopup from "@/components/VideoPopup";

export default function Gallery() {
  const { getImages, videoUrl } = useGallery();

  const [selectedPath, setSelectedPath] = useState(null);
  const [images, setImages] = useState([]);

  const fetchData = async () => {
    const data = await getImages();

    setImages(data);
  };

  useEffect(() => {
    fetchData();
  }, []);

  const photos = [...Array(8).fill("../../assets/photo.svg")];

  const onInit = () => {
    console.log("init");
  };

  return (
    <div className="gallery">
      <div className="subheader">
        <Link href="/?isAnimationFinished=true">
          <Image
            src={require("../../assets/arrow-left.svg")}
            width={24}
            height={24}
          />
        </Link>
        <h2>Public Photos</h2>
        <div></div>
      </div>
      <div className="content">
        {images ? (
          <LightGallery
            onInit={onInit}
            speed={500}
            plugins={[lgThumbnail, lgZoom]}
            thumbnail={true}
            addClass="lg-customize-thumbnails"
          >
            {images?.map(({ image, isVideo, path }) => {
              const url = async () => await videoUrl("video/" + path);

              return isVideo ? (
                <Image
                  src={require("../../assets/video_frame.svg")}
                  className="gallery-photo"
                  onClick={() => setSelectedPath(url())}
                />
              ) : (
                <a href={image}>
                  <img alt="img1" src={image} />
                </a>
              );
            })}
          </LightGallery>
        ) : (
          photos.map((photo) => (
            <Image
              src={require("../../assets/photo.svg")}
              className="gallery-photo"
            />
          ))
        )}
      </div>
      {selectedPath && (
        <VideoPopup path={selectedPath} close={() => setSelectedPath(false)} />
      )}
    </div>
  );
}
