"use client";

const { default: Page } = require("@/components/Page");
const { default: Popup } = require("@/components/Popup");

import { useEffect, useState } from "react";

const Sveta = ({ searchParams }) => {
  const [closePopup, setClosePopup] = useState(false);
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    setMounted(true);
  }, []);

  if (!mounted) return;

  return (
    <>
      {!closePopup ? (
        <Popup setClosePopup={setClosePopup} />
      ) : (
        <Page searchParams={searchParams} isVisitor={true} />
      )}
    </>
  );
};

export default Sveta;
