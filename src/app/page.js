"use client";

import Page from "@/components/Page";
import { useEffect, useState } from "react";

const Home = ({ searchParams }) => {
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    setMounted(true);
  }, []);

  return mounted && <Page searchParams={searchParams} />;
};

export default Home;
