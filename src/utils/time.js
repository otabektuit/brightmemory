export const getTime = (item) => {
  const date = new Date(item * 1000);

  const hours = date.getHours().toString().padStart(2, "0"); // Get hours and pad with leading zero if necessary
  const minutes = date.getMinutes().toString().padStart(2, "0"); // Get minutes and pad with leading zero if necessary

  const formattedTime = hours + ":" + minutes;
  return formattedTime;
};

export const getDate = (item) => {
  const date = new Date(item * 1000);

  const day = date.getDate().toString().padStart(2, "0");
  const month = (date.getMonth() + 1).toString().padStart(2, "0"); // Month is zero-based, so add 1
  const year = date.getFullYear().toString().slice(-2); // Extract the last two digits of the year

  const formattedDate = `${day}/${month}/${year}`;
  return formattedDate;
};

export function isDateOlderThanOneDay(date) {
  const currentDate = new Date();
  const oneDayInMilliseconds = 24 * 60 * 60 * 1000; // Number of milliseconds in a day

  // Calculate the difference in milliseconds between the current date and the given date
  const difference = currentDate - date;

  // Compare the difference with one day in milliseconds
  return difference > oneDayInMilliseconds;
}
