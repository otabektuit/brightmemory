import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyDtUEsErg9etw1TNh_gmmqj4bKcAW50zC4",
  authDomain: "brightmemory-d7cde.firebaseapp.com",
  projectId: "brightmemory-d7cde",
  storageBucket: "brightmemory-d7cde.appspot.com",
  messagingSenderId: "726064165592",
  appId: "1:726064165592:web:aaef8b317773381eb0ddad",
  measurementId: "G-70S4BTN646"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const firestore = getFirestore(app);

export const storage = getStorage(app);
